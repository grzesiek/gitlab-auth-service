package main

import "fmt"

type IdentitiesStorage interface {
	GetUser(string) (User, error)
	GetCell(string) (Cell, error)
	GetDefaultCell() (Cell, error)
	HasDefaultCell() bool
}

func NewIdentitiesStorage() IdentitiesStorage {
	return &MemUserCellsStore{ // TODO we could probably DRY map keys
		Users: map[string]User{
			"grzesiek": User{Name: "grzesiek", Organizations: []string{"default", "organization-1", "organization-2"}, Origin: "default"},
		},
		Cells: map[string]Cell{
			"organization-1": Cell{Name: "gitlab-local-1", Address: "http://gitlab.poc:8081", Organization: Organization{Name: "organization-1"}},
			"organization-2": Cell{Name: "gitlab-local-2", Address: "http://gitlab.poc:8082", Organization: Organization{Name: "organization-2"}},
			"default":        Cell{Name: "gitlab-local-3", Address: "http://gitlab.poc:8083", Organization: Organization{Name: "organization-0"}, Default: true},
		},
	}
}

type MemUserCellsStore struct {
	Users map[string]User
	Cells map[string]Cell
}

func (m *MemUserCellsStore) GetUser(name string) (User, error) {
	user, ok := m.Users[name]
	if ok {
		return user, nil
	}

	return User{}, fmt.Errorf("user not found")
}

func (m *MemUserCellsStore) GetCell(name string) (Cell, error) {
	cell, ok := m.Cells[name]
	if ok {
		return cell, nil
	}

	return Cell{}, fmt.Errorf("cell not found")
}

// TODO this can be implemented better
func (m *MemUserCellsStore) GetDefaultCell() (Cell, error) {
	cell, ok := m.Cells["default"]

	if ok {
		return cell, nil
	}

	return m.Cells["organization-1"], nil
}

// TODO this can be implemented better
func (m *MemUserCellsStore) HasDefaultCell() bool {
	_, ok := m.Cells["default"]

	return ok
}
