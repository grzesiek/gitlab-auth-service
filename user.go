package main

type User struct {
	Name          string
	Origin        string
	Organizations []string
}
