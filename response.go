package main

import (
	"net/http"
	"net/url"
	"strings"

	"golang.org/x/exp/slog"
)

type Response struct {
	res  *http.Response
	user User
	cell Cell
}

// This should not be necessary if a cell has a proper hostname and relative
// URL configured. The redirect would work as expected.
func (r *Response) Rewrite() error {
	location := r.res.Header.Get("Location")
	if len(location) == 0 {
		return nil
	}
	slog.Info("   redirect response", "location", location)

	locationURL, _ := url.Parse(location)
	locationURL.Host = ServiceHost + ServicePort

	// TODO in case of a /users/sign_in redirect, we should redirect user to
	// their origin cell for authorization.

	r.res.Header.Set("Location", locationURL.String())
	slog.Info("   response rewritten", "location", locationURL.String())

	return nil
}

func (r *Response) InterceptCookies() []*http.Cookie {
	cookies := r.res.Cookies()
	if len(cookies) == 0 {
		return nil
	}

	for _, c := range cookies {
		slog.Info("   intercepted cookie", "name", c.Name, "value", c.Value)
	}

	// Remove cookies from the response
	r.res.Header.Del("Set-Cookie")

	return cookies
}

// TODO this should be done differently, by returning a JSON response from a
// cell with session details
func (r *Response) HasAuthenticated() bool {
	if r.res.Request.Method == http.MethodPost && strings.HasSuffix(r.res.Request.URL.String(), "/users/sign_in") && r.res.StatusCode == 302 {
		slog.Info("   detected authenticated response", "path", "/users/sign_in")

		return true
	}

	if r.res.Request.Method == http.MethodPost && strings.HasSuffix(r.res.Request.URL.String(), "/cells/users/auth") && r.res.StatusCode == 201 {
		slog.Info("   detected authenticated response", "path", "/cells/users/auth")

		return true
	}

	return false
}
