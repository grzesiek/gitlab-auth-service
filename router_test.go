package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestFindOrganizationSlugByPath(t *testing.T) {
	router := &Router{Identities: NewIdentitiesStorage()}

	cell, err := router.FindOrganizationSlugByPath("/organization-2/something")
	require.NoError(t, err)
	require.Equal(t, "organization-2", cell)

	cell, err = router.FindOrganizationSlugByPath("organization-1/something/")
	require.NoError(t, err)
	require.Equal(t, "organization-1", cell)

	cell, err = router.FindOrganizationSlugByPath("")
	require.NoError(t, err)
	require.Equal(t, "default", cell)

	cell, err = router.FindOrganizationSlugByPath("/")
	require.NoError(t, err)
	require.Equal(t, "default", cell)

	cell, err = router.FindOrganizationSlugByPath(".")
	require.NoError(t, err)
	require.Equal(t, "default", cell)

	cell, err = router.FindOrganizationSlugByPath("abc")
	require.NoError(t, err)
	require.Equal(t, "default", cell)

}
