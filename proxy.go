package main

import (
	"net/http"
	"net/http/httputil"

	"golang.org/x/exp/slog"
)

type Proxy struct {
	ReverseProxy *httputil.ReverseProxy
	Router       *Router
}

func NewReverseProxy(identities IdentitiesStorage) (*Proxy, error) {
	router := &Router{Identities: identities, Sessions: NewSessionsStorage()}

	proxy := &httputil.ReverseProxy{
		Director:       router.RewriteRequest,
		ModifyResponse: router.RewriteResponse,
	}

	return &Proxy{ReverseProxy: proxy, Router: router}, nil
}

func (p *Proxy) Serve(w http.ResponseWriter, r *http.Request) {
	if p.Router.HasOrganization(r) {
		slog.Info("-> connection received")
		p.ReverseProxy.ServeHTTP(w, r)
	} else {
		p.Router.Redirect(w, r)
	}
}
