package main

import (
	"os"

	"golang.org/x/exp/slog"
)

func main() {
	slog.Info("starting GitLab Auth Service at port " + ServicePort)

	identities := NewIdentitiesStorage()
	proxy, _ := NewReverseProxy(identities)
	service, _ := NewAuthService(proxy)

	err := service.Start()
	if err != nil {
		slog.Error("error starting server", "err", err)

		os.Exit(1)
	}
}
