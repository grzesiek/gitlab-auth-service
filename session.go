package main

import (
	"fmt"
	"net/http"

	"golang.org/x/exp/slog"
)

type SessionsStorage interface {
	GetUserSession(string, string) *Session
	SetUserCookies(string, string, Cookies) error
	MarkAsAuthenticated(string, string) error
	HasAuthSession(string, string) bool
	IsAuthenticated(string) bool
	// GetUserIdentity(string) User
}

type MemSessionsStore struct {
	Identities map[string]User                // Global Identity Session
	Sessions   map[string]map[string]*Session // User sessions in Cells
}

type Cookies []*http.Cookie

type Session struct {
	Cookies       map[string]*http.Cookie
	Authenticated bool
}

func NewSessionsStorage() SessionsStorage {
	return &MemSessionsStore{
		Identities: make(map[string]User),
		Sessions:   make(map[string]map[string]*Session),
	}
}

func (m *MemSessionsStore) GetUserSession(user, cell string) *Session {
	// TODO add an RW mutex
	if m.Sessions[user] == nil {
		return nil
	}

	return m.Sessions[user][cell]
}

func (m *MemSessionsStore) SetUserCookies(user, cell string, cookies Cookies) error {
	if len(cookies) > 0 {
		slog.Info("   storing new session cookies", "cell", cell, "user", user)
	} else {
		return nil
	}

	session := m.GetUserSession(user, cell)
	if session == nil { // TODO read mutex, refactor
		if m.Sessions[user] == nil {
			m.Sessions[user] = make(map[string]*Session)
		}

		if m.Sessions[user][cell] == nil {
			m.Sessions[user][cell] = &Session{Cookies: make(map[string]*http.Cookie)}
		}

		session = m.Sessions[user][cell]
	}

	// TODO write mutex
	for _, c := range cookies {
		slog.Info("   storing cookie", "name", c.Name, "value", c.Value, "user", user)

		session.Cookies[c.Name] = c
	}

	return nil
}

func (m *MemSessionsStore) HasAuthSession(user, cell string) bool {
	session := m.GetUserSession(user, cell)
	if session == nil {
		return false
	}

	return m.IsSignedIn(session)
}

func (m *MemSessionsStore) IsAuthenticated(user string) bool {
	sessions := m.Sessions[user]
	if sessions == nil {
		return false
	}

	for _, session := range sessions {
		if m.IsSignedIn(session) {
			return true
		}
	}

	return false
}

func (m *MemSessionsStore) IsSignedIn(session *Session) bool {
	// TODO we should check session expiration etc etc

	return session.Authenticated
}

func (m *MemSessionsStore) MarkAsAuthenticated(user, cell string) error {
	session := m.GetUserSession(user, cell)
	if session == nil {
		return fmt.Errorf("auth session not found")
	}

	session.Authenticated = true

	return nil
}
