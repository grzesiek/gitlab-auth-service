package main

import (
	"bytes"
	"fmt"
	"io"
	"net/http"
	"path"
	"strings"
	"time"

	"golang.org/x/exp/slog"
)

type Router struct {
	Identities IdentitiesStorage
	Sessions   SessionsStorage
}

func (r *Router) HasOrganization(req *http.Request) bool {
	_, err := r.FindCurrentOrganization(req)

	if err != nil {
		slog.Info("   organization not found", "err", err, "method", req.Method, "url", req.URL.String())

		return false
	}

	return true
}

func (r *Router) Redirect(wrt http.ResponseWriter, req *http.Request) {
	// TODO here we just redirect to the first available organization. Ideally we
	// should redirect to /organizations page and allow user to select the
	// organization they want to connect to.

	cell, _ := r.Identities.GetDefaultCell() // TODO

	slog.Info("<- redirecting to an organization", "cell", cell.Name)

	http.Redirect(wrt, req, "http://"+ServiceHost+ServicePort+"/"+cell.Path(), 302)
}

func (r *Router) RewriteRequest(req *http.Request) {
	user, cell, _ := r.FindUserCell(req) // TODO handle errors

	request := &Request{req: req, user: user, cell: cell}
	slog.Info("-> request", "user", user.Name, "cell", cell.Name, "method", req.Method, "path", request.Path())

	if !r.Sessions.HasAuthSession(user.Name, cell.Slug()) {
		err := r.Authenticate(user, cell)
		if err != nil {
			slog.Error("   cell auth request failed", "err", err)
		}
	}

	session := r.Sessions.GetUserSession(user.Name, cell.Slug())
	if session != nil {
		request.InjectCookies(session.Cookies)
	}

	request.Rewrite()
}

// TODO we could use a transport that is RoundTripper here instead
func (r *Router) RewriteResponse(res *http.Response) error {
	user, cell, _ := r.FindUserCell(res.Request)
	response := &Response{res: res, user: user, cell: cell}

	slog.Info("<- response", "user", user.Name, "cell", cell.Name, "method", res.Request.Method, "url", res.Request.URL.String(), "status", res.StatusCode)

	// mutex
	r.Sessions.SetUserCookies(user.Name, cell.Slug(), response.InterceptCookies())

	// tODO
	if response.HasAuthenticated() {
		r.Sessions.MarkAsAuthenticated(user.Name, cell.Slug())
	}

	return response.Rewrite()
}

func (r *Router) Authenticate(user User, cell Cell) error {
	slog.Info("   authenticate user on cell", "user", user.Name, "cell", cell.Name)

	if !r.Sessions.IsAuthenticated(user.Name) {
		slog.Info("   user not authenticated yet", "user", user.Name, "cell", cell.Name)

		return nil
	}

	reader := bytes.NewReader([]byte(`{"user": "` + user.Name + `"}`))
	req, err := http.NewRequest(http.MethodPost, cell.AuthURL(), reader)
	if err != nil {
		return fmt.Errorf("could not make cell auth request: %s\n", err)
	}
	req.Header.Set("Content-Type", "application/json")

	slog.Info("-> making cell auth request", "user", user.Name, "req", req)

	client := http.Client{Timeout: 5 * time.Second}
	res, err := client.Do(req)
	if err != nil {
		return fmt.Errorf("client: error making http request: %s\n", err)
	}

	body, err := io.ReadAll(res.Body)
	if err != nil {
		slog.Error("   auth response body ready failed", "err", err)
	}

	slog.Info("   auth response", "user", user.Name, "status", res.StatusCode, "body", body)

	r.RewriteResponse(res)

	return nil
}

func (r *Router) FindUserCell(req *http.Request) (User, Cell, error) {
	// find a user by their global identity
	user, _ := r.FindUserByGlobalIdentity("TODO")

	// find the organization we want to route the request to
	cell, err := r.FindCurrentOrganization(req)

	if err == nil {
		slog.Info("   found user and organization", "user", user.Name, "cell", cell.Name)
	}

	return user, cell, err
}

func (r *Router) FindUserByGlobalIdentity(identity string) (User, error) {
	user, _ := r.Identities.GetUser("grzesiek") // TODO

	return user, nil
}

func (r *Router) FindOrganizationSlug(req *http.Request) (string, error) {
	ctxo := req.Context().Value("organization")
	if ctxo != nil {
		return ctxo.(string), nil
	}

	path := path.Clean(req.URL.Path)

	return r.FindOrganizationSlugByPath(path)
}

func (r *Router) FindOrganizationSlugByPath(cpath string) (string, error) {
	ccpath := path.Clean(cpath)
	if strings.HasPrefix(ccpath, "/") || strings.HasPrefix(ccpath, ".") {
		ccpath = ccpath[1:]
	}

	psplit := strings.Split(ccpath, "/")

	if len(psplit) == 0 || len(psplit[0]) == 0 {
		if r.Identities.HasDefaultCell() {
			return "default", nil
		}

		return "", fmt.Errorf("no organization slug found")
	}

	// TODO this needs to be extracted: check if an identifier is a known
	// organization, otherwise it is just a component of a default organization
	cell, _ := r.Identities.GetCell(psplit[0])
	if cell.Slug() == psplit[0] {
		return cell.Slug(), nil
	}

	return "default", nil
}

func (r *Router) FindCurrentOrganization(req *http.Request) (Cell, error) {
	// TODO this is incomplete: check if user has access to a given organization,
	// handle errors, etc.

	slug, _ := r.FindOrganizationSlug(req)

	if len(slug) != 0 {
		return r.Identities.GetCell(slug)
	}

	return Cell{}, fmt.Errorf("cell id not found")
}
