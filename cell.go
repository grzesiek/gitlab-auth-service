package main

import (
	"net/url"

	"golang.org/x/exp/slog"
)

// We are using 1:1 relationship between an organization and a cell here.
type Cell struct {
	Name    string
	Address string
	Default bool
	Organization
}

func (c Cell) URL() (*url.URL, error) {
	url, err := url.Parse(c.Address + c.Path())

	if err != nil {
		slog.Error("cannot get cell address", "err", err)
	}

	return url, err
}

func (c Cell) URLString() string {
	url, _ := c.URL()

	return url.String()
}

func (c Cell) IsDefault() bool {
	return c.Default
}

func (c Cell) Path() string {
	if c.IsDefault() {
		return ""
	} else {
		return "/" + c.Slug()
	}
}

func (c Cell) AuthURL() string {
	return c.URLString() + "/api/v4/cells/users/auth"
}
