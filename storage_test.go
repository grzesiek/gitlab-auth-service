package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestIdentitiesStorage(t *testing.T) {
	store := NewIdentitiesStorage()

	user, err := store.GetUser("grzesiek")
	require.NoError(t, err)
	require.Equal(t, "grzesiek", user.Name)
}

func TestDefaultCell(t *testing.T) {
	store := NewIdentitiesStorage()

	cell, err := store.GetDefaultCell()
	require.NoError(t, err)
	require.Equal(t, "organization-0", cell.Organization.Name)
}
