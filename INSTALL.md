# How to run it?

### Add a custom hostname

Add a custom hostname pointing to the `localhost`. Alternatively you can use
`localhost`, but replace the hostname in the docker commands below.

```
sudo echo "127.0.0.1 gitlab.poc" > /etc/hosts
```

## Install GitLab Cells using Docker

Start GitLab Cell 1 for Organization-1:

```bash
$ docker run --detach \
    --hostname gitlab.poc \
    --publish 4444:443 --publish 8081:80 --publish 2223:22 \
    --name gitlab-local-1 \
    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://gitlab.poc/organization-1';" \
    --env RAILS_RELATIVE_URL_ROOT=/organization-1 \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-1/config:/etc/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-1/logs:/var/log/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-1/data:/var/opt/gitlab:Z \
    --shm-size 256m \
    gitlab/gitlab-ce:latest
```

Start GitLab Cell 2 for Organization-2:

```bash
$ docker run --detach \
    --hostname gitlab.poc \
    --publish 4445:443 --publish 8082:80 --publish 2224:22 \
    --name gitlab-local-2 \
    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://gitlab.poc/organization-2';" \
    --env RAILS_RELATIVE_URL_ROOT=/organization-2 \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-2/config:/etc/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-2/logs:/var/log/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-2/data:/var/opt/gitlab:Z \
    --shm-size 256m \
    gitlab/gitlab-ce:latest
```

To use a "default" organization, start the following container:

```bash
$ docker run --detach \
    --hostname gitlab.poc \
    --publish 4446:443 --publish 8083:80 --publish 2225:22 \
    --name gitlab-local-3 \
    --env GITLAB_OMNIBUS_CONFIG="external_url 'http://gitlab.poc';" \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-3/config:/etc/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-3/logs:/var/log/gitlab:Z \
    --volume /home/grzesiek/tmp-gitlab-cells/gitlab-3/data:/var/opt/gitlab:Z \
    --shm-size 256m \
    gitlab/gitlab-ce:latest
```

## Change root password

Password should be changed on both cells.

You can use following command to read the password:

```bash
$ docker exec gitlab-local-1 cat /etc/gitlab/initial_root_password | grep 'Password:'
```

## Patch GitLab Rails

If you want to try to Cells Single-Sign-On feature, you need to patch your
cells with the following diff:

```diff
diff --git a/lib/api/users.rb b/lib/api/users.rb
index fff0e9fee067..70ce4b637630 100644
--- a/lib/api/users.rb
+++ b/lib/api/users.rb
@@ -21,6 +21,18 @@ class Users < ::API::Base
               /users/:id/custom_attributes/:key
             ]

+    namespace :cells do
+      helpers Devise::Controllers::SignInOut
+      helpers { def session; env['rack.session']; end }
+      helpers { def warden; env['warden']; end }
+
+      post '/users/auth' do
+        user = User.find_by_username(params[:user])
+        sign_in(user) if user.present?
+        { id: user.try(:id) }
+      end
+    end
+
     resource :users, requirements: { uid: /[0-9]*/, id: /[0-9]*/ } do
       include CustomAttributesEndpoints
```

Please note that this is NOT a production ready code!

It will add a simple method to sign in a user with a given username. You can
also simply add this content to the users API source file with `vi`:

```bash
$ vi /opt/gitlab/embedded/service/gitlab-rails/lib/api/users.rb
```

Then restart the services:

```bash
$ docker restart gitlab-local-1 gitlab-local-2 gitlab-local-3
```

## Build and start the service

To build the service run:

```bash
$ go build
```

To start the service run:

```bash
$ ./gitlab-auth-service
```

If you change the port to 80, you may need to run the service using root
account privileges.

## Try it out!

Navigate your browser to `http://gitlab.poc` or `http://gitlab.poc:8080` where
`:8080` is the port number you have used.
