package main

type Organization struct {
	Name string
}

func (o Organization) Slug() string {
	return o.Name
}
