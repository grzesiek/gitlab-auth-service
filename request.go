package main

import (
	"context"
	"net/http"

	"golang.org/x/exp/slog"
)

type Request struct {
	req  *http.Request
	user User
	cell Cell
}

func (r *Request) Path() string {
	return r.req.URL.Path
}

func (r *Request) Rewrite() *http.Request {
	url, _ := r.cell.URL()

	r.req.URL.Scheme = url.Scheme
	r.req.URL.Host = url.Host
	r.req.Host = url.Host

	r.SetOrganizationContext()

	return r.req
}

func (r *Request) SetOrganizationContext() {
	// TODO this should not be a string, but rather a value from a type
	ctx := context.WithValue(r.req.Context(), "organization", r.cell.Slug())

	r.req.WithContext(ctx)
}

func (r *Request) InjectCookies(cookies map[string]*http.Cookie) {
	for _, c := range cookies {
		slog.Info("   setting session cookie", "cookie", c.Name, "user", r.user.Name, "cell", r.cell.Organization)

		r.req.AddCookie(c)
	}
}
