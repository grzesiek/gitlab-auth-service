package main

import (
	"net/http"
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCreateAndGetUserSession(t *testing.T) {
	store := NewSessionsStorage()

	session := store.GetUserSession("grzesiek", "cell-1")
	require.Nil(t, session)

	store.SetUserCookies("grzesiek", "cell-1", []*http.Cookie{&http.Cookie{Name: "test"}})

	session = store.GetUserSession("grzesiek", "cell-1")
	require.NotNil(t, session)
}
