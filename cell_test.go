package main

import (
	"testing"

	"github.com/stretchr/testify/require"
)

func TestCellDefault(t *testing.T) {
	cell := Cell{Organization: Organization{Name: "abc"}}
	require.False(t, cell.IsDefault())
	require.Equal(t, "/abc", cell.Path())

	cell = Cell{Default: true, Organization: Organization{Name: "cde"}}
	require.True(t, cell.IsDefault())
	require.Empty(t, cell.Path())
}
