# Gitlab Auth Service PoC

A small and naive Proof-of-Concept showing how separate GitLab instances can
use a shared auth/routing service built as a reverse proxy.

1. Users have a single global identity stored in an external datastore
   (harcoded in the PoC). The global identity is linked to a list of
   organizations that a user can sign into. The datastore also stores information
   about organizations, cells using this data to route requests.
1. Users authenticate once in their origin organization, what creates a session
   in their origin cell. Once the authentication on the origin cell happens
   successfully, the authentication service marks a user as authenticated and
   creates a global identity session for them.
1. When a user opens a link that points to a different cell, the authentication
   service will create a new session for the user on that cell automatically,
   knowing that the user has been previously authenticated on their origin cell.
1. The user will not need to sign-in again on the second, or any other
   organization they have access too, provided that they have an user account
   there. Creating the accounts automatically is out-of-scope for this PoC.
1. A user can interact with multiple organizations and cells at the same time.
   Organizations are being identified by the first URL path component, which
   can be a top-level group, or a new scope. The authentication service behaves
   like a sessions broker.
1. In the PoC cells are running in separate Docker containers.

**Watch demos:** https://youtu.be/B9ei0F5suDU, https://youtu.be/FYO1MLdZIgg.

## Hypotheses

1. A routing service between Organizations should be aware of user identities.
1. A global identity can link separate, distributed user identities in cells.
1. A global identity might be sufficient to provide a consistent user identity.
1. An identity aware routing service might be able to aggregate cells' data.
1. It might be possible to encode an organization scope into an URL.

## How to try this?

See instructions in `INSTALL.md`.

## Key Learnings

There is a list of things we've learned while working on the PoC. The list is
not complete, as it grows along with the PoC itself.

### 1. Redirects

GitLab often issues a redirect with a `Location` header, especially during the
authentication. It is possible to rewrite response headers to provide a
consistent proxying experience. This is not a problem, but it shows that some
application changes would be required to make cells accessible directly
bypassing the reverse proxy.

### 2. Cloudflare

Cloudflare has checks that try to detect if a reverse proxy is being used
(presumably to detect spoofing). Solutions like Cloudflare should be
implemented in front of the reverse proxy, instead of being placed in between
the proxy and a cell. While it seem obvious, is might be an important detail if
we ever consider using federated authentication solution (for example, for
GitLab Dedicated), where an instance has its own Cloudflare setup, and yet it
needs federated authentication process.

### 3. OAuth / Okta / SAML / SSO

We need to validate this solution with OAuth / Okta / SAML / SSO solutions.

### 4. Organization routing

Switching between Organizations can be implement in multiple ways. One of the
ways is storing the active user session pointer in the GitLab Auth Service
reverse proxy. It would set a desired session depending on the organization
that has been selected by a user. This solution has one drawback: a user can
operate on a single organization at a time. The main benefit is simplicity on
the proxy side and little amount of application changes required.

The single-organization-session solution, however, has less than ideal UX,
because it is a typical workflow that a user wants to operate on different
organizations in different browser tabs. We could make it possible to do it by
reusing an existing GitLab feature: relative URLs. GitLab supports relative
URLs for a while already, as it is possible to install a GitLab on a relative
location, for example,  "my-organization.example/gitlab" It means that most of
the application changes required to make a prepended organization path
component (for example: "gitlab.com/gitlab-inc/gitlab-org/gitlab") working is
already available. Some application changes, however, would be still required
to adapt this for routing.

In this PoC we are exploring the mix of the two solutions.

### 5. Data aggregation

An identity aware (global identity and distributed identity in organizations)
authentication and routing service might be able to perform data aggregation
from many organizations.

Because of being a stateful reverse proxy, it can store information collected
from multiple organizations, keep it updated and maintained (with a specified
interval), and then it can inject aggregated data into a DOM element that has
been provided in response from an organization cell.

This way we should be able to, for example, display a list of assigned issues,
merge requests and TODOs from all of the organizations, even though an HTTP
response comes from a single one.

### 6. Application code changes

Little amount of application code changes would be required to make this
solution production-ready. The current PoC is using separate Docker containers
running unmodified versions of GitLab recently released.

## License

MIT
