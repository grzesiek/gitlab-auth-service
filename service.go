package main

import (
	"io"
	"net/http"
)

const ServiceHost = "gitlab.poc"
const ServicePort = ":8080"

type Service struct {
	Proxy *Proxy
	Muxer *http.ServeMux
}

func NewAuthService(proxy *Proxy) (*Service, error) {
	mux := http.NewServeMux()

	service := &Service{
		Muxer: mux,
		Proxy: proxy,
	}

	mux.HandleFunc("/", service.Roundtrip)
	mux.HandleFunc("/organizations", service.Organizations)
	mux.HandleFunc("/favicon.ico", func(w http.ResponseWriter, r *http.Request) {
		w.WriteHeader(http.StatusNotFound)
	})

	return service, nil
}

func (s *Service) Roundtrip(w http.ResponseWriter, r *http.Request) {
	s.Proxy.Serve(w, r)
}

func (s *Service) Organizations(w http.ResponseWriter, r *http.Request) {
	io.WriteString(w, "Organizations endpoint")
}

func (s *Service) Start() error {
	return http.ListenAndServe(ServicePort, s.Muxer)
}
